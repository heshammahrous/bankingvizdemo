var w = 500,
	h = 500;

var colorscale = d3.scale.category10();

//Legend titles
var LegendOptions = ['Smartphone','Tablet'];

var totalClusterCount=10;

//Final data
var d=[];

//Data


//d.push(dCluster1Mean);
//d.push(dCluster1SD);


//Options for the Radar chart, other than default
var mycfg = {
  w: w,
  h: h,
  maxValue: 0.6,
  minValue: -0.6,
  levels: 3,
  ExtraWidthX: 300
}

//Call function to draw the Radar chart
//Will expect that data is in %'s
//RadarChart.draw("#chart", d, mycfg);

////////////////////////////////////////////
/////////// Initiate legend ////////////////
////////////////////////////////////////////

var svg = d3.select('#body')
	.selectAll('svg')
	.append('svg')
	.attr("width", w+300)
	.attr("height", h)


	
//Create the title for the legend
var text = svg.append("text")
	.attr("class", "title")
	.attr('transform', 'translate(90,0)') 
	.attr("x", w - 70)
	.attr("y", 10)
	.attr("font-size", "12px")
	.attr("fill", "#404040")
	.text("What % of owners use a specific service in a week");
		
//Initiate Legend	
var legend = svg.append("g")
	.attr("class", "legend")
	.attr("height", 100)
	.attr("width", 200)
	.attr('transform', 'translate(90,20)') 
	;

	//Create colour squares
	legend.selectAll('rect')
	  .data(LegendOptions)
	  .enter()
	  .append("rect")
	  .attr("x", w - 65)
	  .attr("y", function(d, i){ return i * 20;})
	  .attr("width", 10)
	  .attr("height", 10)
	  .style("fill", function(d, i){ return colorscale(i);})
	  ;
	//Create text next to squares
	legend.selectAll('text')
	  .data(LegendOptions)
	  .enter()
	  .append("text")
	  .attr("x", w - 52)
	  .attr("y", function(d, i){ return i * 20 + 9;})
	  .attr("font-size", "11px")
	  .attr("fill", "#737373")
	  .text(function(d) { return d; })
	  ;	
 d3.selectAll(".sdCheck").on("change", function() {
  var type = this.value;
  if (this.checked) { // adding data points 

	var localJson=tempJson;
   for(var k in localJson){
	   tempJson[k+"SD"]=PlusSD[k];
	   tempJson[k+"SD2"]=MinusSD[k];
	   
   } 
	
    
  } else { // remove data points from the data 
  
	
   for(var k in tempJson){
	   if(k.includes("SD"))
	   delete tempJson[k];
	   
   } 
  
    
  }
  updateData();
});
var shapeData=[], j=0;
for (var i=0;i<totalClusterCount;i++){
	shapeData.push(i+1);
}

// Create the cluster radio buttons
var form = d3.select("body").append("form");

labels = form.selectAll("label")
    .data(shapeData)
    .enter()
    .append("label")
    .text(function(d) {return d;})
    .insert("input")
    .attr({
        type: "checkbox",
        class: "cluster",
        name: "mode",
        value: function(d, i) {return d;}
    })
    .property("checked", function(d, i) {return i===j;});
	
var tempJson={};
tempJson["1"]=mean["1"];
updateData();

d3.selectAll(".cluster").on("change", function() {
    
if(this.checked){
	
	tempJson[this.value]=mean[this.value]
	
}else{
	delete tempJson[this.value]
	delete tempJson[this.value+"SD"]
	
}
updateData();

    });

function updateData(){
	
	d=[];
   for(var k in tempJson){
	   
	  d.push(tempJson[k]) ;
	  
   } 
	RadarChart.draw("#chart", d, mycfg);
}